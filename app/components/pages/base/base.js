'use strict';

angular.module('myApp.base', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'components/pages/base/base.html',
            controller: 'baseCtrl'
        });

    }])

    .controller('baseCtrl', function () {

    });