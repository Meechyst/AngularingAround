'use strict';

angular.module('myApp.dashboard', ['ngRoute'])

    .config(function ($routeProvider, dateProvider) {

        $routeProvider.when('/dashboard', {
            templateUrl: 'components/pages/dashboard/dashboard.html',
            controller: 'dashboardCtrl'
        });
        var time = dateProvider.$get().devShowDate();
        if (time > 0 && time < 12) {
            dateProvider.setGreet('Good Morning');
        } else if (time > 12 && time < 18) {
            dateProvider.setGreet('Good Evening');
        } else if (time > 18 && time < 20) {
            dateProvider.setGreet('Good Day');
        } else {
            dateProvider.setGreet('Good Night');
        }
    })

    .controller('dashboardCtrl', function ($scope, date) {
        $scope.greetMessage = date.showDate();
        $scope.show = 'msg2';
        $scope.showIt = function(){
          $scope.show = $scope.show == 'msg2' ? 'msg1' : 'msg2';
        };

    });