angular.module('myApp.dashboard').provider('date', function(){
    var greet;
    return {
        //simple setter, this does not initiate untill we call it somewhere
        setGreet: function(val){
            greet = val;
        }, //initiated when the provider is loaded
        $get: function(){
            return { //current hour and a greet message
                showDate: function() {
                    var date = new Date();
                    return "It's "+date.getHours()+" "+greet;
                },
                devShowDate: function(){
                    var date = new Date();
                    return date.getHours();
                }
            }
        }
   }
});