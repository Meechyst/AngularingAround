'use strict';

angular.module('myApp.users', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/users', {
            templateUrl: 'components/pages/users/users.html',
            controller: 'usersCtrl'
        })

    }])

    .controller('usersCtrl', function ($scope, userService) {
        userService.getData().then(function(data) {
            $scope.persons = data.records;
        });


        $scope.addIntoJson = function () {
            $scope.persons.push({'name': $scope.name, 'age': $scope.age, 'username': $scope.username});


            //var dataObj = {
            //    name : $scope.name,
            //    age : $scope.age,
            //    username : $scope.username
            //};
            //$http.post('/db.json', dataObj)
            //.success(function(data) {
            //    $scope.message = data;
            //})
            //    .error(function(data) {
            //    alert( "failure message: " + JSON.stringify({data: data}));
            //});
            //
            //$scope.name='';
            //$scope.age='';
            //$scope.username='';
        };


    });